SELECT *, CASE WHEN RFM = '111' THEN 'Best Customer'
WHEN (F = '1' or F = '2')  AND M='3'  THEN 'Loyal Cheap Customer' 
WHEN F = '1' or F ='2'  THEN 'Loyal Customer'
WHEN RFM = '333' THEN 'Lost Customer'
WHEN  M ='3'  AND (R = '2' or R = '3') or RFM='233' THEN 'Almost Lost'
WHEN M = '1' THEN 'Big Spender'
WHEN M = '2' THEN 'Avg Spender' 
 ELSE 'Lost Cheap Customer' END segment, COUNT(Distinct user_id) users FROM (
SELECT *, R||F||M RFM
FROM
(SELECT *,
CASE WHEN Recency >=10 Then '3' 
WHEN Recency >=5 AND Recency <10 THEN '2' 
WHEN Recency >0 AND Recency<5 THEN '1' ELSE '0' END R, 

CASE WHEN Frequency >0  and Frequency <5 Then '3' 
WHEN Frequency >=5 AND Frequency <10 THEN '2' 
WHEN Frequency >=10 THEN '1' ELSE '0' END F,

CASE WHEN Monetary >0  and Monetary <20 Then '3' 
WHEN Monetary >=20 AND Monetary <40 THEN '2' 
WHEN Monetary >=40 THEN '1' ELSE '0' END M

FROM(
SELECT *,JULIANDAY(first_payment_date) -JULIANDAY(registration_date) AS Recency, 
    SUM(payments_7d) + SUM(payments_8_14d) AS Frequency,  
    SUM(paid_7d) + SUM(paid_8_14d) AS Monetary 
    FROM kit
WHERE is_test_user = 0 and first_payment_date is not null
GROUP BY user_id)
ORDER BY  M desc))
GROUP BY user_id 